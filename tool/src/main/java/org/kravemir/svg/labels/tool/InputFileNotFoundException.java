package org.kravemir.svg.labels.tool;

import java.io.File;

public class InputFileNotFoundException extends RuntimeException {

    private final String parameter;
    private final File file;

    public InputFileNotFoundException(String usage, File file, Throwable cause) {
        super(cause);

        this.parameter = usage;
        this.file = file;
    }

    @Override
    public String getMessage() {
        return String.format("Input for %s \"%s\" not found", parameter, file.getPath());
    }
}
