package org.kravemir.svg.labels.tool;

import org.apache.commons.io.FileUtils;
import org.kravemir.svg.labels.instancing.LabelTemplateDescriptor;
import org.kravemir.svg.labels.tiling.DocumentRenderOptions;
import org.kravemir.svg.labels.tiling.LabelGroup;
import org.kravemir.svg.labels.tiling.TiledPaper;
import org.kravemir.svg.labels.tiling.TiledLabelDocumentsRenderer;
import org.kravemir.svg.labels.tool.common.AbstractCommand;
import org.kravemir.svg.labels.tool.common.PaperOptions;
import org.kravemir.svg.labels.tool.io.FilePersister;
import org.kravemir.svg.labels.tool.tiling.InstancesOptions;
import org.kravemir.svg.labels.tooling.Conventions;
import org.kravemir.svg.labels.tooling.Loader;
import picocli.CommandLine.Command;
import picocli.CommandLine.Mixin;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static java.util.Collections.singletonList;

@Command(
        name = "tile", description = "Tile labels",
        abbreviateSynopsis = true
)
public class TileCommand extends AbstractCommand {

    @Mixin
    private PaperOptions paperOptions;

    @Mixin
    private InstancesOptions instancesOptions;

    @Option(
            names = "--template-descriptor", paramLabel = "FILE",
            description = "Path to JSON file containing descriptor of template"
    )
    private File templateDescriptorFile;

    @Parameters(
            index = "0", paramLabel = "SOURCE",
            description = "Path to SVG file containing a label"
    )
    private File source;

    @Parameters(
            index = "1", paramLabel = "TARGET",
            description = "Path to SVG file which should be generated"
    )
    private File target;


    private final TiledLabelDocumentsRenderer renderer;
    private final Loader loader;
    private final Conventions conventions;

    public TileCommand(TiledLabelDocumentsRenderer renderer) {
        this.renderer = renderer;
        loader = new Loader();
        conventions = new Conventions();
    }


    public void run() {
        try {
            TiledPaper paper = paperOptions.buildPaper();
            List<LabelGroup> labelGroups = loadData();
            DocumentRenderOptions renderOptions = DocumentRenderOptions.newBuilder().build();

            List<String> result = renderer.render(paper, labelGroups, renderOptions);

            new FilePersister().save(target, result);
        } catch (InputFileNotFoundException | InputFileReadFailedException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<LabelGroup> loadData() {
        String svg = loadSourceFile();
        LabelTemplateDescriptor descriptor = loadDescriptor();
        List<LabelGroup.Instance> instances = null;
        try {
            instances = instancesOptions.loadInstances();
        } catch (FileNotFoundException e) {
            throw new InputFileNotFoundException("INSTANCES_DATA", source, e);
        } catch (IOException e) {
            throw new InputFileReadFailedException("INSTANCES_DATA", source, e);
        }

        return singletonList(
                LabelGroup.newBuilder()
                        .setTemplate(svg)
                        .setTemplateDescriptor(descriptor)
                        .addAllInstances(instances)
                        .build()
        );
    }

    private String loadSourceFile() {
        try {
            return FileUtils.readFileToString(source);
        } catch (FileNotFoundException e) {
            throw new InputFileNotFoundException("SVG_SOURCE", source, e);
        } catch (IOException e) {
            throw new InputFileReadFailedException("SVG_SOURCE", source, e);
        }
    }

    private LabelTemplateDescriptor loadDescriptor() {
        if (instancesOptions.hasInstances()) {
            File descriptorFile = getDescriptorFile();

            if (!descriptorFile.exists()) {
                System.err.println("Template descriptor file " + descriptorFile + " doesn't exist.");
                System.exit(1);
            }

            try {
                return loader.loadDescriptor(descriptorFile);
            } catch (FileNotFoundException e) {
                throw new InputFileNotFoundException("DESCRIPTOR", source, e);
            } catch (IOException e) {
                throw new InputFileReadFailedException("DESCRIPTOR", source, e);
            }
        }

        return null;
    }

    private File getDescriptorFile() {
        if (templateDescriptorFile != null) {
            return templateDescriptorFile;
        } else {
            return conventions.resolveDescriptorFileForTemplate(source);
        }
    }
}
