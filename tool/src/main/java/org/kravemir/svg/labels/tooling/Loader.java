package org.kravemir.svg.labels.tooling;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.kravemir.svg.labels.instancing.LabelTemplateDescriptor;
import org.kravemir.svg.labels.tool.model.ReferringLabelGroup;

import java.io.*;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.Validate.isTrue;

public class Loader {

    private static final TypeReference<LinkedHashMap<String, Object>> HASH_MAP_TYPE_REFERENCE = new TypeReference<LinkedHashMap<String, Object>>() {
    };

    private ObjectMapper mapper;

    public Loader() {
    }

    public LabelTemplateDescriptor loadDescriptor(File descriptorFile) throws IOException {
        return getMapper().readValue(descriptorFile, LabelTemplateDescriptor.class);
    }

    public LinkedHashMap<String, String> loadInstance(File datasetJsonFile) throws IOException {
        return getMapper().readValue(datasetJsonFile, HASH_MAP_TYPE_REFERENCE);
    }

    public Map<String, Map<String, String>> loadInstances(Path datasetCSVPath, CSVFormat datasetCSVFormat) throws IOException {
        Reader reader = new BufferedReader(new FileReader(datasetCSVPath.toFile()));
        CSVParser parser = new CSVParser(reader, datasetCSVFormat.withHeader().withIgnoreEmptyLines().withTrim());

        isTrue(parser.getHeaderMap().containsKey("key"), "CSV must contain 'key' column");
        final int headerColumn = parser.getHeaderMap().get("key");

        return parser.getRecords().stream().collect(Collectors.toMap(
                r -> r.get(headerColumn),
                CSVRecord::toMap
        ));
    }

    public void saveInstance(File selectedDatasetFile, LinkedHashMap<String, String> content) throws IOException {
        // TODO: maybe more intelligent to preserve original JSON structure and omit whitespace changes (might need better library)
        getMapper().writerWithDefaultPrettyPrinter().writeValue(selectedDatasetFile, content);
    }

    public ReferringLabelGroup.Instance[] loadInstances(File instancesJsonFile) throws IOException {
        return getMapper().readValue(
                FileUtils.readFileToString(instancesJsonFile),
                ReferringLabelGroup.Instance[].class
        );
    }

    private ObjectMapper getMapper() {
        if (mapper == null) {
            mapper = createMapper();
        }
        return mapper;
    }

    private static ObjectMapper createMapper() {
        DefaultPrettyPrinter.Indenter indenter = new DefaultIndenter("    ", DefaultIndenter.SYS_LF);
        DefaultPrettyPrinter printer = new DefaultPrettyPrinter();
        printer.indentObjectsWith(indenter);
        printer.indentArraysWith(indenter);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        mapper.setDefaultPrettyPrinter(printer);

        org.kravemir.svg.labels.tiling.JacksonMixIns.registerMixIns(mapper);
        org.kravemir.svg.labels.instancing.JacksonMixIns.registerMixIns(mapper);
        org.kravemir.svg.labels.tool.model.JacksonMixIns.registerMixIns(mapper);

        return mapper;
    }
}
