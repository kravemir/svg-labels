package org.kravemir.svg.labels.tool;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LablieCommandLineBuilder {

    private List<String> pieces = new ArrayList<>();

    public static LablieCommandLineBuilder create() {
        return new LablieCommandLineBuilder();
    }

    public TileCommandBuilder tile() {
        pieces.add("tile");

        return new TileCommandBuilder(pieces);
    }

    public  class TileCommandBuilder {
        private final List<String> pieces;

        public TileCommandBuilder(List<String> pieces) {
            this.pieces = pieces;
        }

        public TileCommandBuilder withPaper(String w, String h) {
            pieces.add("--paper-size");
            pieces.add(w);
            pieces.add(h);

            return this;
        }

        public TileCommandBuilder withOffset(String x, String y) {
            pieces.add("--label-offset");
            pieces.add(x);
            pieces.add(y);

            return  this;
        }

        public TileCommandBuilder withLabelSize(String w, String h) {
            pieces.add("--label-size");
            pieces.add(w);
            pieces.add(h);

            return this;
        }

        public TileCommandBuilder withLabelDelta(String x, String y) {
            pieces.add("--label-delta");
            pieces.add(x);
            pieces.add(y);

            return this;
        }

        public TileCommandBuilder withCustom(String... args) {
            pieces.addAll(Arrays.asList(args));

            return this;
        }

        public List<String> getPieces() {
            return pieces;
        }
    }
}
