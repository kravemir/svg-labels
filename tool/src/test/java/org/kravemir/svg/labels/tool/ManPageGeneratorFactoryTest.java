package org.kravemir.svg.labels.tool;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;

import java.time.LocalDate;
import java.time.Month;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class ManPageGeneratorFactoryTest {

    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    private ManPageGeneratorFactory factory;

    @Before
    public void setUp() {
        factory = new ManPageGeneratorFactory();
    }

    @Test
    @Parameters
    public void testManPageDateRespectsDebianEnvironmentVariable(String envVar, LocalDate date) {
        environmentVariables.set("SOURCE_DATE_EPOCH", envVar);

        ManPageGenerator generator = factory.create();

        assertThat(generator.getManPageDate(), is(date));
    }

    public static Object[][] parametersForTestManPageDateRespectsDebianEnvironmentVariable() {
        return new Object[][] {
                {"1568481791", LocalDate.of(2019, Month.SEPTEMBER, 14)},
                {"1549074191", LocalDate.of(2019, Month.FEBRUARY, 2)}
        };
    }
}
