package org.kravemir.svg.labels.tool;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.contrib.java.lang.system.SystemErrRule;
import org.junit.rules.TemporaryFolder;

import java.io.File;

public class AbstractToolRunnerTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public final SystemErrRule systemErrRule = new SystemErrRule().enableLog();

    protected File outputFile = null;

    @Before
    public void setUp() throws Exception {
        outputFile = folder.newFile("testOutput.svg");
        outputFile.delete();
    }

    @After
    public void logOutput() throws Exception {
        if (outputFile.exists()) {
            System.out.println(FileUtils.readFileToString(outputFile));
        } else {
            System.out.println("Output file wasn't created: " + outputFile.getAbsolutePath());
        }
    }
}
