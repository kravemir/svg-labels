package org.kravemir.svg.labels.tool;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.kravemir.svg.labels.TemplateResoures;
import org.kravemir.svg.labels.util.ListBuilder;
import org.w3c.dom.Node;

import java.io.File;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.kravemir.svg.labels.TemplateResoures.*;
import static org.kravemir.svg.labels.matcher.NodesMatchingXPath.nodesMatchingXPath;
import static org.kravemir.svg.labels.tool.FileContainsSvgMatcher.isSvgFileThat;

public class ToolRunnerInstancingTest extends AbstractToolRunnerTest {

    @Test
    public void testRenderWithoutInstance() {
        List<String> options = baseTileOptions()
                .withCustom(
                        TEMPLATE_01.getAsFile(folder::newFile).getAbsolutePath(),
                        outputFile.getAbsolutePath()
                )
                .getPieces();

        runTool(options);

        assertThat(outputFile, isSvgFileThat(
                nodesMatchingXPath("/*/*", Matchers.<Collection<Node>>allOf(
                        hasSize(33),
                        everyItem(allOf(TEMPLATE_01_MATCHER, not(TEMPLATE_01_DATA_01_MATCHER), not(TEMPLATE_02_MATCHER)))
                ))
        ));
    }

    @Test
    public void testRenderWithInstance() {
        List<String> options = baseTileOptions()
                .withCustom(
                        "--instance-json",
                        TemplateResoures.DATA_01.getAsFile(folder::newFile).getAbsolutePath(),
                        "--template-descriptor",
                        TemplateResoures.TEMPLATE_01_DESCRIPTOR.getAsFile(folder::newFile).getAbsolutePath(),
                        TemplateResoures.TEMPLATE_01.getAsFile(folder::newFile).getAbsolutePath(),
                        outputFile.getAbsolutePath()
                )
                .getPieces();

        runTool(options);

        assertThat(outputFile, isSvgFileThat(
                nodesMatchingXPath("/*/*", Matchers.<Collection<Node>>allOf(
                        hasSize(33),
                        everyItem(allOf(TEMPLATE_01_DATA_01_MATCHER, not(TEMPLATE_01_MATCHER), not(TEMPLATE_02_MATCHER)))
                ))
        ));
    }

    @Test
    public void testRenderData01FromCSVDataSet() {
        List<String> options = baseTileOptions()
                .withCustom(
                        "--dataset-csv",
                        TemplateResoures.INSTANCES_CSV.getAsFile(folder::newFile).getAbsolutePath(),
                        "--instance",
                        "data_01",
                        "--template-descriptor",
                        TemplateResoures.TEMPLATE_01_DESCRIPTOR.getAsFile(folder::newFile).getAbsolutePath(),
                        TemplateResoures.TEMPLATE_01.getAsFile(folder::newFile).getAbsolutePath(),
                        outputFile.getAbsolutePath()
                )
                .getPieces();

        runTool(options);

        assertThat(outputFile, isSvgFileThat(
                nodesMatchingXPath("/*/*", Matchers.<Collection<Node>>allOf(
                        hasSize(33),
                        everyItem(allOf(TEMPLATE_01_DATA_01_MATCHER, not(TEMPLATE_01_MATCHER), not(TEMPLATE_02_MATCHER)))
                ))
        ));
    }

    @Test
    public void testRenderData02FromCSVDataSet() {
        List<String> options = baseTileOptions()
                .withCustom(
                        "--dataset-csv",
                        TemplateResoures.INSTANCES_CSV.getAsFile(folder::newFile).getAbsolutePath(),
                        "--instance",
                        "data_02",
                        "--template-descriptor",
                        TemplateResoures.TEMPLATE_01_DESCRIPTOR.getAsFile(folder::newFile).getAbsolutePath(),
                        TemplateResoures.TEMPLATE_01.getAsFile(folder::newFile).getAbsolutePath(),
                        outputFile.getAbsolutePath()

                ).getPieces();

        runTool(options);

        assertThat(outputFile, isSvgFileThat(
                nodesMatchingXPath("/*/*", Matchers.<Collection<Node>>allOf(
                        hasSize(33),
                        everyItem(allOf(TEMPLATE_01_DATA_02_MATCHER, not(TEMPLATE_01_MATCHER), not(TEMPLATE_02_MATCHER)))
                ))
        ));
    }

    @Test
    public void testRenderWithInstances() {
        List<String> options = baseTileOptions()
                .withCustom(
                        "--instances-json",
                        TemplateResoures.INSTANCES_01.getAsFile(folder::newFile).getAbsolutePath(),
                        "--template-descriptor",
                        TemplateResoures.TEMPLATE_01_DESCRIPTOR.getAsFile(folder::newFile).getAbsolutePath(),
                        TemplateResoures.TEMPLATE_01.getAsFile(folder::newFile).getAbsolutePath(),
                        outputFile.getAbsolutePath()
                ).getPieces();

        runTool(options);


        assertThat(outputFile, isSvgFileThat(
                nodesMatchingXPath("/*/*", Matchers.<Collection<Node>>allOf(
                        hasSize(33),
                        Matchers.contains(
                                TEMPLATE_01_INSTANCES_01_MATCHER_LIST.subList(0, 33)
                        )
                ))
        ));
    }

    @Test
    public void testRenderMultiplePagesWithInstances() {
        List<String> options = LablieCommandLineBuilder.create()
                .tile()
                .withPaper("210", "297")
                .withOffset("0", "0")
                .withLabelSize("200", "26.5")
                .withLabelDelta("0", "0")
                .withCustom(
                        "--instances-json",
                        TemplateResoures.INSTANCES_01.getAsFile(folder::newFile).getAbsolutePath(),
                        "--template-descriptor",
                        TemplateResoures.TEMPLATE_01_DESCRIPTOR.getAsFile(folder::newFile).getAbsolutePath(),
                        TemplateResoures.TEMPLATE_01.getAsFile(folder::newFile).getAbsolutePath(),
                        outputFile.getAbsolutePath()
                ).getPieces();

        runTool(options);

        assertThat(new File(outputFile.getAbsoluteFile().getParent(), "testOutput.0.svg"), isSvgFileThat(nodesMatchingXPath("/*/*", Matchers.<Collection<Node>>allOf(
                hasSize(11),
                Matchers.contains(new ListBuilder<Matcher<? super Node>>()
                        // TODO: refactor these matchers
                        .add(1, allOf(
                                nodesMatchingXPath(".//*[@id='nameText']/*[1][text()='Test name']", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='nameText']/*[2][not(text())]", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='nameText']/*", hasSize(2)),
                                nodesMatchingXPath(".//*[@id='text4540']/*[text()='Test description']", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='text4544']/*[text()='19. 05. 2018']", hasSize(1))
                        ))
                        .add(1, allOf(
                                nodesMatchingXPath(".//*[@id='nameText']/*[1][text()='Test name 2']", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='nameText']/*[2][not(text())]", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='nameText']/*", hasSize(2)),
                                nodesMatchingXPath(".//*[@id='text4540']/*[text()='Test description 2']", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='text4544']/*[text()='15. 07. 2018']", hasSize(1))
                        ))
                        .add(9, allOf(
                                nodesMatchingXPath(".//*[@id='nameText']/*[1][text()='Test rest']", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='nameText']/*[2][not(text())]", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='nameText']/*", hasSize(2)),
                                nodesMatchingXPath(".//*[@id='text4540']/*[text()='Fill the rest of the page']", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='text4544']/*[text()='15. 07. 2018']", hasSize(1))
                        ))
                        .build()
                )
        ))));

        assertThat(new File(outputFile.getAbsoluteFile().getParent(), "testOutput.1.svg"), isSvgFileThat(nodesMatchingXPath("/*/*", Matchers.<Collection<Node>>allOf(
                hasSize(11),
                Matchers.contains(new ListBuilder<Matcher<? super Node>>()
                        .add(11, allOf(
                                nodesMatchingXPath(".//*[@id='nameText']/*[1][text()='Test rest']", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='nameText']/*[2][not(text())]", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='nameText']/*", hasSize(2)),
                                nodesMatchingXPath(".//*[@id='text4540']/*[text()='Fill the rest of the page']", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='text4544']/*[text()='15. 07. 2018']", hasSize(1))
                        ))
                        .build()
                )
        ))));

        assertThat(new File(outputFile.getAbsoluteFile().getParent(), "testOutput.2.svg"), isSvgFileThat(nodesMatchingXPath("/*/*", Matchers.<Collection<Node>>allOf(
                hasSize(11),
                Matchers.contains(new ListBuilder<Matcher<? super Node>>()
                        .add(11, allOf(
                                nodesMatchingXPath(".//*[@id='nameText']/*[1][text()='Test rest']", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='nameText']/*[2][not(text())]", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='nameText']/*", hasSize(2)),
                                nodesMatchingXPath(".//*[@id='text4540']/*[text()='Fill the rest of the page']", hasSize(1)),
                                nodesMatchingXPath(".//*[@id='text4544']/*[text()='15. 07. 2018']", hasSize(1))
                        ))
                        .build()
                )
        ))));
    }

    private void runTool(List<String> options) {
        ToolRunner.main(options.toArray(new String[0]));
    }

    private LablieCommandLineBuilder.TileCommandBuilder baseTileOptions() {
        return LablieCommandLineBuilder.create()
                .tile()
                .withPaper("210", "297")
                .withOffset("0", "0")
                .withLabelSize("65", "26.5")
                .withLabelDelta("0", "0");
    }

}
